import aws_cdk as cdk
import aws_cdk.assertions as assertions

from aws_cdk_work.aws_cdk_work_stack import AwsCdkWorkStack

# example tests. To run these tests, uncomment this file along with the example
# resource in aws_cdk_work/aws_cdk_work_stack.py

def _network_fw(type):
    app = cdk.core.App()
    stack = AwsCdkWorkStack(app, "aws-cdk-work")
    template = assertions.Template.from_stack(stack)
    
    template.has_resource_properties("AWS::NetworkFirewall::RuleGroup", {
        "Type": type
    })
def test_vpc_created():
    app = cdk.core.App()
    stack = AwsCdkWorkStack(app, "aws-cdk-work")
    template = assertions.Template.from_stack(stack)

    template.has_resource_properties("AWS::EC2::VPC", {
        "CidrBlock": "10.3.0.0/24"
    })
    
def test_public_subnet_created():
    app = cdk.core.App()
    stack = AwsCdkWorkStack(app, "aws-cdk-work")
    template = assertions.Template.from_stack(stack)
    
    template.has_resource_properties("AWS::EC2::Subnet",{
        "MapPublicIpOnLaunch": True
    })
    
def test_private_subnet_created():
    app = cdk.core.App()
    stack = AwsCdkWorkStack(app, "aws-cdk-work")
    template = assertions.Template.from_stack(stack)
    
    template.has_resource_properties("AWS::EC2::Subnet",{
        "MapPublicIpOnLaunch": False
    })
    
def test_private_subnet_created():
    app = cdk.core.App()
    stack = AwsCdkWorkStack(app, "aws-cdk-work")
    template = assertions.Template.from_stack(stack)
    
    template.has_resource_properties("AWS::EC2::Subnet",{
        "MapPublicIpOnLaunch": False
    })
    
def test_instance_role_created():
    app = cdk.core.App()
    stack = AwsCdkWorkStack(app, "aws-cdk-work")
    template = assertions.Template.from_stack(stack)
    
    template.has_resource_properties("AWS::EC2::Instance", {
        "KeyName": "default"
    })
    
def test_networkfw_rule_stateful_created():
    _network_fw("STATEFUL")
    
def test_loggroup_created():
    app = cdk.core.App()
    stack = AwsCdkWorkStack(app, "aws-cdk-work")
    template = assertions.Template.from_stack(stack)
    
    template.has_resource_properties("AWS::Logs::LogGroup", {
        "RetentionInDays": 1
    })
    
def test_flow_log_created():
    app = cdk.core.App()
    stack = AwsCdkWorkStack(app, "aws-cdk-work")
    template = assertions.Template.from_stack(stack)
    
    template.has_resource_properties("AWS::EC2::FlowLog", {
        "ResourceType": "VPC",
        "TrafficType": "ALL"
    })

