from aws_cdk import (
    aws_ec2 as ec2,
    aws_cloudwatch as cloudwatch,
    aws_networkfirewall as networkfirewall,
    aws_logs as logs,
    aws_iam as iam,
    core
)
from constructs import Construct
from requests import get

"""
Get the public IP of the host instance
"""
my_ip = get('https://api.ipify.org').text


"""
AWS Provide micro instance free for 12 months from date of account activation.
"""
ec2_type="t3.micro"

"""
Key to be used for instance creation and login This should already be present.
"""
instance_key_name = "default"

"""
AMI to be used for instance creation.
Indicate your AMI, no need a specific id in the region.
"""
linux_ami = ec2.AmazonLinuxImage(
    generation=ec2.AmazonLinuxGeneration.AMAZON_LINUX,
    edition=ec2.AmazonLinuxEdition.STANDARD,
    virtualization=ec2.AmazonLinuxVirt.HVM,
    storage=ec2.AmazonLinuxStorage.GENERAL_PURPOSE,
)

"""
CIDR Range
"""
cidr_range = "10.3.0.0/24"

class AwsCdkWorkStack(core.Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        
        env_name = "test"
        
        # Create Vpc
        self.vpc = ec2.Vpc(self, "{}-vpc".format(env_name),
            cidr = cidr_range,
            max_azs = 2,
            enable_dns_hostnames = True,
            enable_dns_support = True, 
            subnet_configuration=[
                ec2.SubnetConfiguration(
                    name = '{}-Public-Subnet'.format(env_name),
                    subnet_type = ec2.SubnetType.PUBLIC,
                    cidr_mask = 26
                ),
                ec2.SubnetConfiguration(
                    name = '{}-Private-Subnet'.format(env_name),
                    subnet_type = ec2.SubnetType.PRIVATE,
                    cidr_mask = 26
                )
            ],
            nat_gateways = 1
        )
        
        # Create Security group for the instance
        self.security_group = ec2.SecurityGroup(self, "{}-bastion-sec-grp".format(env_name),
            vpc = self.vpc,
            allow_all_outbound=True,
            description = 'Security group for a bastion host'
        )
        
        # Create Instance Role
        self.instance_role = iam.Role(self, "{}-instance-role".format(env_name),
            assumed_by = iam.ServicePrincipal("ec2.amazonaws.com")
        )
        #add managed policy
        self.instance_role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name("AmazonSSMManagedInstanceCore"))
        
        # Create Instance
        self.instance = ec2.Instance(self, "{}-instance".format(env_name),
            vpc = self.vpc,
            key_name = instance_key_name,
            machine_image = linux_ami,
            vpc_subnets = ec2.SubnetSelection(
                subnet_type = ec2.SubnetType.PUBLIC,
            ),
            instance_name = "{}-bastion-sec-grp".format(env_name),
            instance_type = ec2.InstanceType(instance_type_identifier = ec2_type),
            security_group = self.security_group,
            role = self.instance_role,
            user_data = self.instance_user_data("./userdata")
        )
        
        # Add Ingress rule to allow SSH
        self.security_group.add_ingress_rule(
            ec2.Peer().ipv4(my_ip+'/32'),
            ec2.Port.tcp(22),
            description = "Allow SSH Traffic from {}/32 IP".format(my_ip)
        )
        
        # Network Firewall ICMP rule
        self.icmp_rule = networkfirewall.CfnRuleGroup(self, "{}-nwfw-rule-icmp".format(env_name),
            capacity = 100,
            rule_group_name = "{}-nwfw-rule-icmp".format(env_name),
            type = "STATEFUL",
            rule_group = networkfirewall.CfnRuleGroup.RuleGroupProperty(
                rules_source = networkfirewall.CfnRuleGroup.RulesSourceProperty(
                    stateful_rules = [
                        networkfirewall.CfnRuleGroup.StatefulRuleProperty(
                            action = "ALERT",
                            header = networkfirewall.CfnRuleGroup.HeaderProperty(
                                direction = "ANY",
                                protocol = "ICMP",
                                destination = "ANY",
                                source = "ANY",
                                destination_port = "ANY",
                                source_port = "ANY"
                            ),
                            rule_options = [
                                networkfirewall.CfnRuleGroup.RuleOptionProperty(
                                    keyword = "sid:1"
                                )
                            ]
                        )
                    ]
                )
            )
        )
        
        # Domain names to be added to allowed rule
        my_target = [".amazon.com"]
        
        # Allow http and https traffic
        self.allowed_rule = networkfirewall.CfnRuleGroup(self, "{}-allow-rule".format(env_name),
            capacity = 100,
            rule_group_name = "{}-allow-rule".format(env_name),
            type = "STATEFUL",
            rule_group = networkfirewall.CfnRuleGroup.RuleGroupProperty(
                rules_source = networkfirewall.CfnRuleGroup.RulesSourceProperty(
                    rules_source_list = networkfirewall.CfnRuleGroup.RulesSourceListProperty(
                        generated_rules_type = "ALLOWLIST",
                        target_types = [
                            "HTTP_HOST",
                            "TLS_SNI",
                        ],
                        targets = my_target,
                    )
                ),
                rule_variables = networkfirewall.CfnRuleGroup.RuleVariablesProperty(
                    ip_sets = {
                        "HOME_SET": networkfirewall.CfnRuleGroup.IPSetProperty(
                            definition = [cidr_range]
                        )
                    }
                ),
            )
        )

        # Network Firewall Policy creation
        self.network_firewall_policy = networkfirewall.CfnFirewallPolicy(self, "{}-vpc-firewall-policy".format(env_name),
            firewall_policy = networkfirewall.CfnFirewallPolicy.FirewallPolicyProperty(
                stateless_default_actions=["aws:forward_to_sfe"],
                stateless_fragment_default_actions = ["aws:forward_to_sfe"],
                stateful_rule_group_references = [
                    networkfirewall.CfnFirewallPolicy.StatefulRuleGroupReferenceProperty(resource_arn=self.icmp_rule.ref),
                    networkfirewall.CfnFirewallPolicy.StatefulRuleGroupReferenceProperty(resource_arn=self.allowed_rule.ref)
                ]
            ),
            firewall_policy_name = "{}-vpc-firewall-policy".format(env_name)
        )
        
        # Now Create the Network firewall
        self.network_firewall = networkfirewall.CfnFirewall(self, "{}-vpc-firewall".format(env_name),
            firewall_name = "{}-vpc-firewall".format(env_name),
            firewall_policy_arn = self.network_firewall_policy.ref,
            subnet_mappings = [networkfirewall.CfnFirewall.SubnetMappingProperty(
                subnet_id = subnet_id
            )for subnet_id in self.vpc.select_subnets(subnet_group_name='{}-Public-Subnet'.format(env_name)).subnet_ids],
            vpc_id = self.vpc.vpc_id
        )
        
        # Log Group setup for cloud watch logs
        self.log_group = self.log_setup("{}-vpc-flow-log".format(env_name)) # VPC Flow log
        self.network_log_group = self.log_setup("{}-vpc-network-log-flow".format(env_name)) # Network FLow log
        self.network_log_group_alert = self.log_setup("{}-vpc-network-log-alert".format(env_name)) # Network alert log
        
        # Role for VPC log flow
        self.flowlog_role = iam.Role(self, "{}-flowlog-role".format(env_name),
            assumed_by = iam.ServicePrincipal("vpc-flow-logs.amazonaws.com")
        )
        
        # Add AWS Managed policy to Role
        self.flowlog_role.add_managed_policy(
            iam.ManagedPolicy.from_aws_managed_policy_name("CloudWatchLogsFullAccess"),
        )
        
        # Enable VPC Flow log delivery to CloudWatch Log
        self.cfn_flow_log = ec2.CfnFlowLog(
            self,
            "{}-vpc-flowlog".format(env_name),
            resource_id = self.vpc.vpc_id,
            resource_type = 'VPC',
            traffic_type = 'ALL',
            deliver_logs_permission_arn = self.flowlog_role.role_arn,
            log_destination = self.log_group.log_group_arn,
            log_destination_type = 'cloud-watch-logs'
        )
        
        # Enable Network firewall logging
        self.network_log_config = networkfirewall.CfnLoggingConfiguration(self, "{}-network-fw-log".format(env_name),
            firewall_arn = self.network_firewall.ref,
            logging_configuration = networkfirewall.CfnLoggingConfiguration.LoggingConfigurationProperty(
                log_destination_configs = [
                    networkfirewall.CfnLoggingConfiguration.LogDestinationConfigProperty(
                        log_destination={
                            "logGroup": self.network_log_group_alert.log_group_name
                        },
                        log_destination_type = "CloudWatchLogs",
                        log_type = "ALERT"
                    ),
                    networkfirewall.CfnLoggingConfiguration.LogDestinationConfigProperty(
                        log_destination={
                            "logGroup": self.network_log_group.log_group_name
                        },
                        log_destination_type = "CloudWatchLogs",
                        log_type = "FLOW"
                    )
                ]
            ),
            firewall_name = "{}-vpc-firewall".format(env_name)
        )
        
        # Create DashBoard
        self.vpc_dashboard = cloudwatch.Dashboard(self, "{}-vpc-dashboard".format(env_name),
            dashboard_name = "{}-vpc-dashboard".format(env_name)
        )
        
        # add log query widget for flow logs
        self.vpc_dashboard.add_widgets(self.log_query_widget(self.log_group.log_group_name))
        
        # Metrics for graph
        stateless_a_metric = []
        stateless_a_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'ReceivedPackets',
                {"FirewallName": self.network_log_group.log_group_name, "AvailabilityZone": self.region+"a", "Engine": "Stateless"},
                "Received Packets",
                900
            )
        )
        stateless_a_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'PassedPackets',
                {"FirewallName": self.network_log_group.log_group_name, "AvailabilityZone": self.region+"a", "Engine": "Stateless"},
                "Passed Packets",
                900
            )
        )
        stateless_a_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'DroppedPackets',
                {"FirewallName": self.network_log_group_alert.log_group_name, "AvailabilityZone": self.region+"a", "Engine": "Stateless"},
                "Dropped Packets",
                900
            )
        )
        
        stateful_a_metric = []
        stateful_a_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'ReceivedPackets',
                {"FirewallName": self.network_log_group.log_group_name, "AvailabilityZone": self.region+"a", "Engine": "Stateful"},
                "Received Packets",
                900
            )
        )
        stateful_a_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'PassedPackets',
                {"FirewallName": self.network_log_group.log_group_name, "AvailabilityZone": self.region+"a", "Engine": "Stateful"},
                "Passed Packets",
                900
            )
        )
        stateful_a_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'DroppedPackets',
                {"FirewallName": self.network_log_group_alert.log_group_name, "AvailabilityZone": self.region+"a", "Engine": "Stateful"},
                "Dropped Packets",
                900
            )
        )
        
        stateless_b_metric = []
        stateless_b_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'ReceivedPackets',
                {"FirewallName": self.network_log_group.log_group_name, "AvailabilityZone": self.region+"b", "Engine": "Stateless"},
                "Received Packets",
                900
            )
        )
        stateless_b_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'PassedPackets',
                {"FirewallName": self.network_log_group.log_group_name, "AvailabilityZone": self.region+"b", "Engine": "Stateless"},
                "Passed Packets",
                900
            )
        )
        stateless_b_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'DroppedPackets',
                {"FirewallName": self.network_log_group_alert.log_group_name, "AvailabilityZone": self.region+"b", "Engine": "Stateless"},
                "Dropped Packets",
                900
            )
        )
        
        stateful_b_metric = []
        stateful_b_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'ReceivedPackets',
                {"FirewallName": self.network_log_group.log_group_name, "AvailabilityZone": self.region+"b", "Engine": "Stateful"},
                "Received Packets",
                900
            )
        )
        stateful_b_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'PassedPackets',
                {"FirewallName": self.network_log_group.log_group_name, "AvailabilityZone": self.region+"b", "Engine": "Stateful"},
                "Passed Packets",
                900
            )
        )
        stateful_b_metric.append(
            self.metric_widget(
                'AWS/NetworkFirewall',
                'DroppedPackets',
                {"FirewallName": self.network_log_group_alert.log_group_name, "AvailabilityZone": self.region+"b", "Engine": "Stateful"},
                "Dropped Packets",
                900
            )
        )
        
        # Add widgets
        self.vpc_dashboard.add_widgets(
            cloudwatch.GraphWidget(
                title = "Stateless Zone A",
                width = 6,
                stacked = True,
                left = stateless_a_metric
            )
        )
        
        self.vpc_dashboard.add_widgets(
            cloudwatch.GraphWidget(
                title = "Stateful Zone A",
                width = 6,
                stacked = True,
                left = stateful_a_metric
            )
        )
        
        self.vpc_dashboard.add_widgets(
            cloudwatch.GraphWidget(
                title = "Stateless Zone b",
                width = 6,
                stacked = True,
                left = stateless_b_metric
            )
        )
        
        self.vpc_dashboard.add_widgets(
            cloudwatch.GraphWidget(
                title = "Stateful Zone b",
                width = 6,
                stacked = True,
                left = stateful_b_metric
            )
        )
        
        core.CfnOutput(
            self,
            "CloudWatchDashboardURL",
            value="https://{}.console.aws.amazon.com/cloudwatch/home?region={}#dashboards:name={}-vpc-dashboard".format(self.region, self.region, env_name),
            export_name = "CloudWatchDashboardURL"
        )

    def instance_user_data(self, filename):
        data = ec2.UserData.for_linux()
        with open(filename, 'r') as file:
            data.add_commands(file.read())
        return data
        
    def log_setup(self, name):
        return logs.LogGroup(self, name,
            log_group_name = name,
            retention = logs.RetentionDays.ONE_DAY,
            removal_policy = core.RemovalPolicy.DESTROY
        )
        
    def log_query_widget(self, name):
        return cloudwatch.LogQueryWidget(
            log_group_names = [name],
            view=cloudwatch.LogQueryVisualizationType.TABLE,
            query_lines = [
                'fields @timestamp, @message',
                'sort @timestamp desc',
                'limit 20'
            ],
            width = 24,
            height = 6
        )
        
    def metric_widget(self, namespace, metric_name, dimension, label_name = None, second = 900):
        return cloudwatch.Metric(
            metric_name = metric_name,
            namespace = namespace,
            dimensions_map = dimension,
            unit = cloudwatch.Unit.COUNT,
            label = label_name,
            statistic = "sum",
            period = core.Duration.seconds(second)
        )