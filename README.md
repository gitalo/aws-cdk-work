
# Aws CDK Work

*** This is very crude use at your own risk!!! ;) ***

This is a Vpc project for Python development with CDK.

## Install instruction

Once checked out manually create a virtualenv on MacOS and Linux:

```
$ python3 -m venv .venv
```

Once the virtualenv is created, you can use the following
step to activate your virtualenv.

```
$ source .venv/bin/activate
```

If you are a Windows platform, you would activate the virtualenv like this:

```
% .venv\Scripts\activate.bat
```

Once the virtualenv is activated, you can install the required dependencies.

```
$ pip install -r requirements.txt
```

At this point you can now synthesize the CloudFormation template for this code.

```
$ cdk synth
```

To deploy you need do the following:

* Create below environment variable
* * CDK_DEFAULT_ACCOUNT should point to your AWS account
* * CDK_DEFAULT_REGION should point to your default AWS region
* Setup you aws credentials

Once above steps are done run the following command

```
$ cdk deploy --profile <profile_name>
```
This will create VPC with subnets in 2 AZs with network firewall and dashboard.

*** Note: ***
You can change the following variable as per your need.

* ec2_type default is t3.micro
* instance_key_name the key present in your account
* cidr_range default is 10.3.0.0/24
