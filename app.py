#!/usr/bin/env python3
import os

import aws_cdk as cdk

from aws_cdk_work.aws_cdk_work_stack import AwsCdkWorkStack


app = cdk.core.App()

# keeping it environment-agnostic.
AwsCdkWorkStack(app, "AwsCdkWorkStack", env=cdk.core.Environment(
    account=os.environ.get("CDK_DEPLOY_ACCOUNT", os.environ["CDK_DEFAULT_ACCOUNT"]),
    region=os.environ.get("CDK_DEPLOY_REGION", os.environ["CDK_DEFAULT_REGION"])))

app.synth()
